from bs4 import BeautifulSoup
import requests
import random
# import os

url_base = "https://alpha.wallhaven.cc/search?q="
url_tail = "&sorting=favorites&order=desc"
url_wallpaper = "https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-"

query = ["minimalist", "galaxy", "triangle", "prism", "geometry", "abstract", "code", "quote"]

url = url_base + query[random.randint(0, len(query)-1)] + url_tail
print("Request: " + url)

req = requests.get(url)
soup = BeautifulSoup(req.text, 'html.parser')
wallpaper_containers = soup.find_all('figure')
# for i in range(len(wallpaper_containers)):
#     print(wallpaper_containers[i])
wallpaper_number  = wallpaper_containers[random.randint(0, len(wallpaper_containers)-1)].a['href'].split('/')[-1]

print("Downloading wallpaper number : " + wallpaper_number)
img_type = '.jpg'
print("Downloading image in format: " + img_type)
img_req = requests.get(url_wallpaper + wallpaper_number + img_type)
if img_req.status_code != requests.codes.ok:
    img_type = '.png'
    print("Download failed. Trying image in format: " + img_type)
    img_req = requests.get(url_wallpaper + wallpaper_number + img_type)

file_name = "wallhaven-" + wallpaper_number + img_type
with open(file_name, 'wb') as f:
    f.write(img_req.content)
print("Download Successful!!")

# print(os.getcwd())
